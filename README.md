# Transposer Utils

Collection of modules, helper functions and regex patterns.

All *Transposer Services* assume these to be installed.


## Install

To install, use the *Shared Library (shared-lib)* template project and clone this one into the `modules` directory. If you follow the installation instructions of the service(s) you intend to use and `shared-lib`, the service(s) will pick up the module automatically.

### Dependencies

To install the dependencies,
```bash
pip install -r requirements.txt
```
inside each service's virtual environment that uses the `shared-lib` this is installed into.
