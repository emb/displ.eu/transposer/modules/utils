#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import typing

import re
import json

import configparser
from deepmerge import always_merger

from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END
)

RE_REPLACE_FOR_ENVIRON = re.compile(r'[ \t\n\r\.-]', re.I)



# Parse value from configparser or environment
def _parse_value(val: typing.Any) -> typing.Any:
    if RE_TRUE.match(val):
        return True
    if RE_FALSE.match(val):
        return False
    if RE_INT.match(val):
        return int(val)
    if RE_FLOAT.match(val):
        return float(val)
    if RE_NONE.match(val):
        return None
    if (
        (
            RE_JSON_OBJECT_START.search(val) and
            RE_JSON_OBJECT_END.search(val)
        ) or (
            RE_JSON_ARRAY_START.search(val) and
            RE_JSON_ARRAY_END.search(val)
        )
    ):
        try:
            return json.loads(val)
        except json.JSONDecodeError as err:
            return val
    
    return val



class TrpConfigSection:
    def __init__(self, parser: configparser.ConfigParser, section: str, options: dict, use_environ: bool, environ_prefix: str) -> None:
        self._parser = parser
        self._name = section
        self._name_formated = RE_REPLACE_FOR_ENVIRON.sub('_', section).upper() + '__'
        self._use_environ = use_environ
        self._environ_prefix = environ_prefix
        self._options = {}
        _opts = options.copy()
        
        # Parse options inside section
        for option in parser.options(section):
            val = parser[section][option]
            if use_environ:
                env_val_name = RE_REPLACE_FOR_ENVIRON.sub('_', option).lower()
                if env_val_name in _opts:
                    del _opts[env_val_name]
                #env_name = environ_prefix + self._name_formated + RE_REPLACE_FOR_ENVIRON.sub('_', option).upper()
                env_name = environ_prefix + self._name_formated + env_val_name.upper()
                if env_name in os.environ:
                    val = os.environ[env_name]
            self._options[option] = _parse_value(val)
        
        # Parse options from '_section_keys' option
        if use_environ and '_section_keys' in self._options:
            for k in self._options['_section_keys']:
                env_val_name = RE_REPLACE_FOR_ENVIRON.sub('_', k).lower()
                if env_val_name in _opts:
                    del _opts[env_val_name]
                env_name = environ_prefix + self._name_formated + env_val_name.upper()
                if env_name not in os.environ:
                    continue
                val = os.environ[env_name]
                self._options[k] = _parse_value(val)
        
        # Parse manually passed in options that are left
        if len(_opts.keys()) > 0:
            for k, val in _opts.items():
                self._options[k] = _parse_value(val)
        
        # Remove '_section_keys' option as it is not needed anymore
        try:
            del self._options['_section_keys']
        except KeyError as err:
            pass
    
    
    # Implement Iterator
    def __iter__(self):
        """Initialize iterator"""
        self._option_items = list(self._options.items())
        return self

    def __next__(self):
        """Iterate over options as tuples"""
        try:
            return self._option_items.pop(0)
        except IndexError as err:
            raise StopIteration
    
    
    @property
    def name(self) -> str:
        """Get section name"""
        return self._name
    
    
    @property
    def options(self) -> dict:
        """Get options as dict"""
        return self._options
    
    
    def get(self, name: str, default: typing.Any = None) -> typing.Any:
        """Get option by name"""
        if name in self._options:
            return self._options[name]
        return default
    
    
    def has(self, name) -> bool:
        """Test if option exists in section"""
        return (name in self._options)



class TrpConfigSectionParserless:
    def __init__(self, section: str, options: dict) -> None:
        self._name = section
        self._options = {}
        
        # Parse options inside section
        for option_k, val in options.items():
            self._options[option_k] = _parse_value(val)
    
    
    # Implement Iterator
    def __iter__(self):
        """Initialize iterator"""
        self._option_items = list(self._options.items())
        return self

    def __next__(self):
        """Iterate over options as tuples"""
        try:
            return self._option_items.pop(0)
        except IndexError as err:
            raise StopIteration
    
    
    @property
    def name(self) -> str:
        """Get section name"""
        return self._name
    
    
    @property
    def options(self) -> dict:
        """Get options as dict"""
        return self._options
    
    
    def get(self, name: str, default: typing.Any = None) -> typing.Any:
        """Get option by name"""
        if name in self._options:
            return self._options[name]
        return default
    
    
    def has(self, name) -> bool:
        """Test if option exists in section"""
        return (name in self._options)



class TrpConfig:
    def __init__(self, config: dict | None = None, use_environ: bool = False, environ_prefix: str | None = None) -> None:
        self._has_config = False
        if config is None:
            config = {}
        self._config = always_merger.merge({
            'defaults': {
                'dir': None,
                'file': 'defaults.ini',
                'data': None
            },
            'files': None
        }, config)
        
        self._use_environ = use_environ
        self._environ_prefix = environ_prefix
        if not environ_prefix:
            self._environ_prefix = ''
        if self._use_environ and self._environ_prefix:
            self._environ_prefix += '__'
        
        self._parser = configparser.ConfigParser()
        self._sections = {}
        environ_sections = {}
        if self._use_environ:
            if self._environ_prefix:
                re_prefix = re.compile(r'^{}'.format(self._environ_prefix), re.I)
                for k in os.environ:
                    if k.startswith(self._environ_prefix):
                        k_t = re_prefix.sub('', k)
                        k_elems = k_t.split('__')
                        if len(k_elems) < 2:
                            continue
                        k_section = k_elems.pop(0).lower()
                        k_option = k_elems.pop(0).lower()
                        if k_section not in environ_sections:
                            environ_sections[k_section] = {}
                        environ_sections[k_section][k_option] = os.environ[k]
            else:
                for k in os.environ:
                    k_elems = k.split('__')
                    if len(k_elems) < 2:
                        continue
                    k_section = k_elems.pop(0).lower()
                    k_option = k_elems.pop(0).lower()
                    if k_section not in environ_sections:
                        environ_sections[k_section] = {}
                    environ_sections[k_section][k_option] = os.environ[k]
        
        if isinstance(self._config['defaults']['data'], dict):
            self._parser.read_dict(self._config['defaults']['data'])
            self._has_config = True
        
        if (
            isinstance(self._config['defaults']['dir'], str) and
            self._config['defaults']['dir'] and
            isinstance(self._config['defaults']['file'], str) and
            self._config['defaults']['file']
        ):
            file_path = os.path.abspath(os.path.join(self._config['defaults']['dir'], self._config['defaults']['file']))
            if os.path.isfile(file_path):
                with open(file_path, 'r') as fh:
                    self._parser.read_file(fh)
                    self._has_config = True
        
        if isinstance(self._config['files'], list):
            result = self._parser.read(self._config['files'])
            if isinstance(result, list) and len(result) > 0:
                self._has_config = True
        
        if self._has_config:
            for section in self._parser.sections():
                _opts = {}
                if section in environ_sections:
                    _opts = environ_sections[section]
                    del environ_sections[section]
                self._sections[section] = TrpConfigSection(
                    self._parser,
                    section,
                    _opts,
                    self._use_environ,
                    self._environ_prefix
                )
            if len(environ_sections.keys()) > 0:
                for section, options in environ_sections.items():
                    self._sections[section] = TrpConfigSectionParserless(
                        section,
                        options
                    )
    
    
    
    # Implement Iterator (for sections)
    def __iter__(self):
        """Initialize iterator"""
        self._section_items = list(self._sections.items())
        return self

    def __next__(self):
        """Iterate over sections as tuples"""
        try:
            return self._section_items.pop(0)
        except IndexError as err:
            raise StopIteration
    
    
    
    @property
    def has_config(self) -> bool:
        """Test if config was loaded"""
        return self._has_config
    
    
    @property
    def sections(self) -> dict:
        """Get sections as dict"""
        return self._sections
    
    
    def get(self, name: str) -> TrpConfigSection | None:
        """Get section by name"""
        if name in self._sections:
            return self._sections[name]
        return None
    
    
    def has(self, name: str) -> bool:
        """Test if section exists in config"""
        return (name in self._sections)





####################################################################################################
# Testing

if __name__ == '__main__':
    files = ['tests/test_config.ini']
    config = TrpConfig({
        'defaults': {
            'dir': '../etc',
            'file': 'defaults.ini',
            'data': None
        },
        'files': files
    }, True, 'TRP')
    
    
    print('config has_config:', config.has_config)
    print('config.sections:', config.sections)
    
    for key, val in config:
        print('config iter:', val.name, '=', val.options)
        for k, v in val:
            print('    section iter:', k, '=', v)


