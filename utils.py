#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================

import os
import typing

import datetime
import re
import json

import uuid
import shortuuid


# Regex patterns
RE_TRUE = re.compile(r'^(?:true|yes|ja|y|j|1)$', re.I)
RE_FALSE = re.compile(r'^(?:false|no|nein|n|0)$', re.I)
RE_NONE = re.compile(r'^(?:none|null)$', re.I)
RE_NEGATED = re.compile(r'^\s*!\s*')

RE_NUMBER = re.compile(r'^-?\d+(?:\.\d+)?$', re.I)
RE_NUMBER_WCOMMA = re.compile(r'^-?\d+(?:,\d+)?$', re.I)
RE_NUMBER_WCOMMA2 = re.compile(r'^-?\d+(?:\.\d{3})*(?:,\d+)?$', re.I)
RE_INT = re.compile(r'^-?\d+$', re.I)
RE_FLOAT = re.compile(r'^-?\d+\.\d+$', re.I)
RE_FLOAT_WCOMMA = re.compile(r'^-?\d+,\d+$', re.I)
RE_FLOAT_WCOMMA2 = re.compile(r'^-?\d+(?:\.\d{3})*,\d+$', re.I)
RE_FLOAT_WCOMMA3 = re.compile(r'^-?\d+(?:,\d{3})+\.\d+$', re.I)

RE_RATIO = re.compile(r'^(-?\d+(?:\.\d+)?)\s*[:/]\s*(-?\d+(?:\.\d+)?)$', re.I)

RE_PRIMITIVE = re.compile(r'^(?:(?:-?\d+(?:\.\d+)?)|true|yes|ja|y|j|false|no|nein|n|none|null)$', re.I)

RE_FRACTION = re.compile(r'^(\d+)(?:/|:)(\d+)$', re.I)
RE_SPLIT_FRACTION = re.compile(r'[/:]', re.I)

RE_JSON_OBJECT_START = re.compile(r'^\s*\{', re.I)
RE_JSON_OBJECT_END = re.compile(r'\}\s*$', re.I)
RE_JSON_ARRAY_START = re.compile(r'^\s*\[', re.I)
RE_JSON_ARRAY_END = re.compile(r'\]\s*$', re.I)

RE_COMMA_SEPARATOR = re.compile(r'\s*,\s*', re.I)
RE_COMMA_TRAILING = re.compile(r',\s*$', re.I)

RE_NEWLINE_SINGLE = re.compile(r'[\n\r]', re.I)
RE_NEWLINE_MULTI = re.compile(r'[\n\r]+', re.I)
RE_WHITESPACE = re.compile(r'[\s\n\r]+', re.I)
RE_WHITESPACE_START = re.compile(r'^[\s\n\r]+', re.I)
RE_WHITESPACE_END = re.compile(r'[\s\n\r]+$', re.I)

RE_HHMM = re.compile(r'^(\d+):(\d{1,2})$', re.I)
RE_HHMMSS = re.compile(r'^(\d+):(\d{1,2}):(\d{1,2})$', re.I)
RE_HHMMSS_STRICT = re.compile(r'^(\d+):(\d{2}):(\d{2})$', re.I)
RE_HHMMSSMS = re.compile(r'^(\d+):(\d{1,2}):(\d{1,2})[:\.](\d{1,6})$', re.I)
RE_HHMMSSMS_ALT = re.compile(r'^_(\d{2,6})_(\d{2})_(\d{2})_(\d{3,6})$', re.I)
RE_HHMMSSMS_STRICT = re.compile(r'^(\d+):(\d{2}):(\d{2})[\.:](\d{1,6})$', re.I)

RE_DATE_F1 = re.compile(r'^(\d{4})\s*[/\.-]\s*(\d{2})\s*[/\.-]\s*(\d{2})$', re.I)
RE_DATE_F2 = re.compile(r'^(\d{1,2})\s*[/\.-]\s*(\d{1,2})\s*[/\.-]\s*(\d{4})$', re.I)

RE_TIME_F1 = re.compile(r'^(\d{1,2})\s*[\.:]\s*(\d{1,2})$', re.I)
RE_TIME_F2 = re.compile(r'^(\d{1,2})\s*[\.:]\s*(\d{1,2})\s*[\.:]\s*(\d{1,2})$', re.I)
RE_TIME_F3 = re.compile(r'^(\d{1,2})\s*[\.:]\s*(\d{1,2})\s*[\.:]\s*(\d{1,2})\.(\d{1,6})$', re.I)

RE_DATETIME_F11 = re.compile(r'^(\d{4})\s*[/\.-]\s*(\d{2})\s*[/\.-]\s*(\d{2})(?:\s+|T|-)(\d{1,2})\s*[\.:]\s*(\d{1,2})$', re.I)
RE_DATETIME_F12 = re.compile(r'^(\d{4})\s*[/\.-]\s*(\d{2})\s*[/\.-]\s*(\d{2})(?:\s+|T|-)(\d{1,2})\s*[\.:]\s*(\d{1,2})\s*[\.:]\s*(\d{1,2})$', re.I)
RE_DATETIME_F13 = re.compile(r'^(\d{4})\s*[/\.-]\s*(\d{2})\s*[/\.-]\s*(\d{2})(?:\s+|T|-)(\d{1,2})\s*[\.:]\s*(\d{1,2})\s*[\.:]\s*(\d{1,2})\.(\d{1,6})$', re.I)
RE_DATETIME_F21 = re.compile(r'^(\d{1,2})\s*[/\.-]\s*(\d{1,2})\s*[/\.-]\s*(\d{4})(?:\s+|T|-)(\d{1,2})\s*[\.:]\s*(\d{1,2})$', re.I)
RE_DATETIME_F22 = re.compile(r'^(\d{1,2})\s*[/\.-]\s*(\d{1,2})\s*[/\.-]\s*(\d{4})(?:\s+|T|-)(\d{1,2})\s*[\.:]\s*(\d{1,2})\s*[\.:]\s*(\d{1,2})$', re.I)
RE_DATETIME_F23 = re.compile(r'^(\d{1,2})\s*[/\.-]\s*(\d{1,2})\s*[/\.-]\s*(\d{4})(?:\s+|T|-)(\d{1,2})\s*[\.:]\s*(\d{1,2})\s*[\.:]\s*(\d{1,2})\.(\d{1,6})$', re.I)
RE_DATETIME_F01 = re.compile(r'^(UTC|GMT) (\d{4})-(\d{2})-(\d{2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$', re.I)

RE_STARTSWITH_SLASH = re.compile(r'\s*/', re.I)
RE_STARTSWITH_TILDE = re.compile(r'\s*~', re.I)

RE_SURROUND_TYPES = re.compile(r'^(\d{1,2}\.\d|mono|stereo|quad)(?:\(\w+\))?$', re.I)
SURROUND_CHANNELS_MAP = {
    'mono': 1,
    'stereo': 2,
    'quad': 4,
    '3.0': 3,
    '5.0': 5,
    '5.1': 6,
    '6.0': 6,
    '6.1': 7,
    '7.0': 7,
    '7.1': 8,
    '8.0': 8,
    '8.1': 9,
    
    1: 'mono',
    2: 'stereo',
    3: '3.0',
    4: 'quad',
    5: '5.0',
    6: '5.1',
    7: '6.1',
    8: '7.1',
    9: '8.1'
}



def get_uuid() -> str:
    """
    Get unique id
    
        Returns:
            str: Unique id
    """
    return shortuuid.uuid()


def get_uuidv4() -> str:
    """
    Get uuid v4
    
        Returns:
            str: Unique id
    """
    return str(uuid.uuid4())


def to_bool(
        val: typing.Any,
        default: bool | None = None) -> bool:
    """
    Convert to boolean
    
        Parameters:
            val (typing.Any): Value to convert
            default (bool | None): Default value if conversion fails
        
        Returns:
            bool: Converted value
    """
    if isinstance(val, bool):
        return val
    if isinstance(val, str):
        if RE_TRUE.match(val):
            return True
        if RE_FALSE.match(val):
            return False
        if default is not None:
            return default
    try:
        return bool(val)
    except:
        if default is not None:
            return default
        return False


def is_false(
        val: typing.Any) -> bool:
    """
    Test if value is false
    
        Parameters:
            val (typing.Any): Value to test
        
        Returns:
            bool: Result
    """
    if val is None:
        return True
    if isinstance(val, bool):
        return not val
    if isinstance(val, (int, float)):
        return val == 0
    if isinstance(val, str) and (not val or RE_FALSE.match(val)):
        return True
    return False


def is_true(
        val: typing.Any) -> bool:
    """
    Test if value is true
    
        Parameters:
            val (typing.Any): Value to test
        
        Returns:
            bool: Result
    """
    if val is None:
        return False
    if isinstance(val, bool):
        return val
    if isinstance(val, (int, float)):
        return val != 0
    if isinstance(val, str) and RE_TRUE.match(val):
        return True
    return False


def to_tristate(
        val: typing.Any,
        default: bool | None = None) -> bool | None:
    """
    Convert to tri-state (True, False, None)
    
        Parameters:
            val (typing.Any): Value to convert
            default (bool | None): Default value if conversion fails
        
        Returns:
            bool | None: Converted value
    """
    if val is None:
        return None
    if isinstance(val, bool):
        return val
    if isinstance(val, str):
        if RE_TRUE.match(val):
            return True
        if RE_FALSE.match(val):
            return False
        if RE_NONE.match(val):
            return None
        return default
    if isinstance(val, (int, float)):
        if val > 0:
            return True
        if val == 0:
            return False
    return default


def to_number(
        val: typing.Any,
        default: int | float | None = 0) -> int | float | None:
    """
    Convert to number
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | float | None): Default value if conversion fails
        
        Returns:
            int | float | None: Converted value
    """
    if isinstance(val, str):
        if RE_INT.match(val):
            return int(val)
        if RE_FLOAT.match(val):
            return float(val)
        if RE_FLOAT_WCOMMA.match(val):
            val = val.replace(',', '.')
            return float(val)
        if RE_FLOAT_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return float(val)
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return float(val)
    if isinstance(val, (int, float)):
        return val
    return default


def to_int(
        val: typing.Any,
        default: int | None = 0) -> int | None:
    """
    Convert to integer
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | None): Default value if conversion fails
        
        Returns:
            int | None: Converted value
    """
    if isinstance(val, str):
        if RE_NUMBER.match(val):
            return int(val)
        if RE_NUMBER_WCOMMA.match(val):
            val = val.replace(',', '.')
            return int(val)
        if RE_NUMBER_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return int(val)
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return int(val)
    if isinstance(val, (int, float)):
        return int(val)
    return default


def to_int_or_bool(
        val: typing.Any,
        default: int | bool | None = 0) -> int | bool | None:
    """
    Convert to integer or boolean
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | bool | None): Default value if conversion fails
        
        Returns:
            int | bool | None: Converted value
    """
    if isinstance(val, str):
        if RE_NUMBER.match(val):
            return int(val)
        if RE_NUMBER_WCOMMA.match(val):
            val = val.replace(',', '.')
            return int(val)
        if RE_NUMBER_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return int(val)
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return int(val)
        if RE_TRUE.match(val):
            return True
        if RE_FALSE.match(val):
            return False
        return default
    if isinstance(val, (int, float)):
        return int(val)
    if isinstance(val, bool):
        return val
    return default


def to_float(
        val: typing.Any,
        default: float | None = 0.0) -> float | None:
    """
    Convert to float
    
        Parameters:
            val (typing.Any): Value to convert
            default (float | None): Default value if conversion fails
        
        Returns:
            float | None: Converted value
    """
    if isinstance(val, str):
        if RE_NUMBER.match(val):
            return float(val)
        if RE_NUMBER_WCOMMA.match(val):
            val = val.replace(',', '.')
            return float(val)
        if RE_NUMBER_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return float(val)
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return float(val)
    if isinstance(val, (int, float)):
        return float(val)
    return default


def to_float_or_bool(
        val: typing.Any,
        default: float | bool | None = 0) -> float | bool | None:
    """
    Convert to float or boolean
    
        Parameters:
            val (typing.Any): Value to convert
            default (float | bool | None): Default value if conversion fails
        
        Returns:
            float | bool | None: Converted value
    """
    if isinstance(val, str):
        if RE_NUMBER.match(val):
            return float(val)
        if RE_NUMBER_WCOMMA.match(val):
            val = val.replace(',', '.')
            return float(val)
        if RE_NUMBER_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return float(val)
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return float(val)
        if RE_TRUE.match(val):
            return True
        if RE_FALSE.match(val):
            return False
        return default
    if isinstance(val, (int, float)):
        return float(val)
    if isinstance(val, bool):
        return val
    return default


def is_positive(
        val: typing.Any,
        default: bool | None = False) -> bool | None:
    """
    Convert to abs value
    
        Parameters:
            val (typing.Any): Value to convert
            default (bool | None): Default value if conversion fails
        
        Returns:
            float | None: Converted value
    """
    if isinstance(val, str):
        if RE_NUMBER.match(val):
            return float(val) >= 0
        if RE_NUMBER_WCOMMA.match(val):
            val = val.replace(',', '.')
            return float(val) >= 0
        if RE_NUMBER_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return float(val) >= 0
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return float(val) >= 0
        return default
    if isinstance(val, (int, float)):
        return (val >= 0)
    return default


def is_negative(
        val: typing.Any,
        default: bool | None = False) -> bool | None:
    """
    Convert to abs value
    
        Parameters:
            val (typing.Any): Value to convert
            default (bool | None): Default value if conversion fails
        
        Returns:
            float | None: Converted value
    """
    if isinstance(val, str):
        if RE_NUMBER.match(val):
            return float(val) < 0
        if RE_NUMBER_WCOMMA.match(val):
            val = val.replace(',', '.')
            return float(val) < 0
        if RE_NUMBER_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return float(val) < 0
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return float(val) < 0
        return default
    if isinstance(val, (int, float)):
        return (val < 0)
    return default


def get_numerator(
        val: typing.Any,
        default: int | float | None = None) -> int | float | None:
    """
    Extract numerator from value
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | float | None): Default value if conversion fails
        
        Returns:
            int | float | None: Converted value
    """
    if isinstance(val, (int, float)):
        return val
    if isinstance(val, str):
        result = RE_FRACTION.match(val)
        if not result:
            return default
        return int(result.group(1))
    return default


def get_denominator(
        val: typing.Any,
        default: int | float | None = None) -> int | float | None:
    """
    Extract denominator from value
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | float | None): Default value if conversion fails
        
        Returns:
            int | float | None: Converted value
    """
    if isinstance(val, (int, float)):
        return val
    if isinstance(val, str):
        result = RE_FRACTION.match(val)
        if not result:
            return default
        return int(result.group(2))
    return default


def to_abs(
        val: typing.Any,
        default: int | float | None = None) -> int | float | None:
    """
    Convert to abs value
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | float | None): Default value if conversion fails
        
        Returns:
            float | None: Converted value
    """
    if isinstance(val, str):
        if RE_INT.match(val):
            return abs(int(val))
        if RE_FLOAT.match(val):
            return abs(float(val))
        if RE_FLOAT_WCOMMA.match(val):
            val = val.replace(',', '.')
            return abs(float(val))
        if RE_FLOAT_WCOMMA2.match(val):
            val = val.replace('.', '')
            val = val.replace(',', '.')
            return abs(float(val))
        if RE_FLOAT_WCOMMA3.match(val):
            val = val.replace(',', '')
            return abs(float(val))
        return default
    if isinstance(val, (int, float)):
        return abs(val)
    return default


def to_str(
        val: typing.Any,
        default: str | None = None) -> str | None:
    """
    Convert to string
    
        Parameters:
            val (typing.Any): Value to convert
            default (str | None): Default value if conversion fails
        
        Returns:
            str | None: Converted value
    """
    if isinstance(val, str):
        return val
    if isinstance(val, (int, float, bool)):
        return str(val)
    return default


def to_str_from_selection(
        val: typing.Any,
        selection: str | None = None,
        default: str | None = None) -> str | None:
    """
    Validate string is in selection
    
        Parameters:
            val (typing.Any): Value to convert
            selection (str | None): Selection of values
            default (str | None): Default value if conversion fails
        
        Returns:
            str | None: Converted value
    """
    if val is None or selection is None:
        return default
    
    if isinstance(val, str):
        if re.match(r'^(?:{})$'.format(selection), val, re.I):
            return val
    return default


def is_empty(val: typing.Any) -> bool:
    """
    Test if value is empty
    
        Parameters:
            val (typing.Any): Value to test
        
        Returns:
            bool: Result
    """
    if val is None:
        return True
    if isinstance(val, str):
        if len(val) == 0:
            return True
        return False
    if isinstance(val, list):
        if len(val) == 0:
            return True
        return False
    if isinstance(val, dict):
        if len(val.keys()) == 0:
            return True
        return False
    return False


def str_is_empty(val: typing.Any) -> bool:
    """
    Test if string is empty
    
        Parameters:
            val (typing.Any): Value to test
        
        Returns:
            bool: Result
    """
    if val is None:
        return True
    if not isinstance(val, str):
        return True
    if len(val) == 0:
        return True
    return False

def str_is_not_empty(val: typing.Any) -> bool:
    return not str_is_empty(val)


def to_dict(
        val: typing.Any,
        default: dict[str, typing.Any] | None = None) -> dict[str, typing.Any] | None:
    """
    Convert to dict
    
        Parameters:
            val (typing.Any): Value to convert
            default (dict | None): Default value if conversion fails
        
        Returns:
            dict | None: Converted value
    """
    if isinstance(val, str):
        try:
            result = json.loads(val)
            if isinstance(result, dict):
                return result
            return default
        except:
            return default
    if isinstance(val, dict):
        return val
    return default


def to_dict_or_bool(
        val: typing.Any,
        default: dict[str, typing.Any] | None = None) -> dict[str, typing.Any] | bool | None:
    """
    Convert to dict
    
        Parameters:
            val (typing.Any): Value to convert
            default (dict | None): Default value if conversion fails
        
        Returns:
            dict | None: Converted value
    """
    if isinstance(val, str):
        if RE_TRUE.match(val):
            return True
        if RE_FALSE.match(val):
            return False
        try:
            result = json.loads(val)
            if isinstance(result, dict):
                return result
            return default
        except:
            return default
    if isinstance(val, dict):
        return val
    if isinstance(val, bool):
        return val
    if isinstance(val, (int, float)):
        return val > 0
    return default


def to_list(
        val: typing.Any,
        default: list | None = None,
        separator: str | None = None) -> list | None:
    """
    Convert to list
    
        Parameters:
            val (typing.Any): Value to convert
            default (list | None): Default value if conversion fails
            separator (str | None): Separator to split string
        
        Returns:
            list | None: Converted value
    """
    if isinstance(val, str):
        if (
            (
                RE_JSON_OBJECT_START.search(val) and
                RE_JSON_OBJECT_END.search(val)
            ) or (
                RE_JSON_ARRAY_START.search(val) and
                RE_JSON_ARRAY_END.search(val)
            )
        ):
            try:
                result = json.loads(val)
                if isinstance(result, list):
                    return result
                return default
            except:
                return default
        
        if separator:
            separator = re.compile(r'\s*{}\s*'.format(separator), re.I)
        else:
            separator = RE_COMMA_SEPARATOR
        return separator.split(val)

    if isinstance(val, list):
        return val
    return default


def to_list_from_selection(
        val: list | str | int | float | bool | None,
        selection: str | None = None,
        default: list | None = None,
        separator: str | None = None) -> list | None:
    """
    Convert to list
    
        Parameters:
            val (list | str | int | float | bool | None): Value to convert
            selection (str | None): Selection of values
            default (list | None): Default value if conversion fails
            separator (str | None): Separator to split string
        
        Returns:
            list | None: Converted value
    """
    if isinstance(val, str):
        if (
            (
                RE_JSON_OBJECT_START.search(val) and
                RE_JSON_OBJECT_END.search(val)
            ) or (
                RE_JSON_ARRAY_START.search(val) and
                RE_JSON_ARRAY_END.search(val)
            )
        ):
            try:
                result = json.loads(val)
                if isinstance(result, list):
                    pat = re.compile(r'^(?:{})$'.format(selection), re.I)
                    result = [x for x in result if isinstance(x, str) and pat.match(x)]
                    return result
                return default
            except:
                return default
        
        if separator:
            separator = re.compile(r'\s*{}\s*'.format(separator), re.I)
        else:
            separator = RE_COMMA_SEPARATOR
        result = separator.split(val)
        pat = re.compile(r'^(?:{})$'.format(selection), re.I)
        result = [x for x in result if isinstance(x, str) and pat.match(x)]
        return result

    if isinstance(val, list):
        pat = re.compile(r'^(?:{})$'.format(selection), re.I)
        result = [x for x in val if isinstance(x, str) and pat.match(x)]
        return result
    return default


def remove_trailling_comma(val: typing.Any) -> str:
    """
    Remove trailing comma from string.
    
        Parameters:
            val (str): Value to sanitize
        
        Returns:
            str: Sanitized value
    """
    if not isinstance(val, str):
        return ''
    return val.rstrip(',')


def to_msecs(val: typing.Any) -> int:
    """
    Convert to milliseconds from time string.
    Acceptable formats:
        HH:MM:SS:MS
        HH:MM:SS.MS
        HH:MM:SS
        HH:MM
    If value is an int or float, it is returned as-is.
    All other values are converted to 0.
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            int: Converted value
    """
    if isinstance(val, (int, float)):
        return val
    if not isinstance(val, str):
        return 0
    
    result = RE_HHMMSSMS.match(val)
    if result:
        ms = result.group(4)
        ms = ms[0:3]
        ms = ms.ljust(3, '0')
        return (int(result.group(1)) * 3600000) + \
            (int(result.group(2)) * 60000) + \
            (int(result.group(3)) * 1000) + \
            int(ms)
    
    result = RE_HHMMSS.match(val)
    if result:
        return (int(result.group(1)) * 3600000) + \
            (int(result.group(2)) * 60000) + \
            (int(result.group(3)) * 1000)
    
    result = RE_HHMM.match(val)
    if result:
        return (int(result.group(1)) * 3600000) + \
            (int(result.group(2)) * 60000)
    
    return 0


def to_musecs(val: typing.Any) -> int:
    """
    Convert to microseconds from time string.
    Acceptable formats:
        HH:MM:SS:uS
        HH:MM:SS.uS
        HH:MM:SS
        HH:MM
    If value is an int or float, it is returned as-is.
    All other values are converted to 0.
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            int: Converted value
    """
    if isinstance(val, (int, float)):
        return val
    if not isinstance(val, str):
        return 0
    
    result = RE_HHMMSSMS.match(val)
    if result:
        us = result.group(4)
        us = us.ljust(6, '0')
        return (int(result.group(1)) * 3600000000) + \
            (int(result.group(2)) * 60000000) + \
            (int(result.group(3)) * 1000000) + \
            int(us)
    
    result = RE_HHMMSS.match(val)
    if result:
        return (int(result.group(1)) * 3600000000) + \
            (int(result.group(2)) * 60000000) + \
            (int(result.group(3)) * 1000000)
    
    result = RE_HHMM.match(val)
    if result:
        return (int(result.group(1)) * 3600000000) + \
            (int(result.group(2)) * 60000000)
    
    return 0


def frames_to_msecs(
        val: typing.Any,
        fps: int | float = 25) -> int | float:
    """
    Convert to milliseconds from number of frames.
    
        Parameters:
            val (typing.Any): Number of frames
            fps (int | float): Frames per second
        
        Returns:
            int | float: Sanitized value
    """
    if isinstance(val, (int, float)):
        return (val / fps) * 1000
    if not isinstance(val, str):
        return 0
    if RE_INT.match(val):
        return (int(val) / fps) * 1000
    if RE_FLOAT.match(val):
        return (float(val) / fps) * 1000
    if RE_FLOAT_WCOMMA.match(val):
        val = val.replace(',', '.')
        return (float(val) / fps) * 1000
    if RE_FLOAT_WCOMMA2.match(val):
        val = val.replace('.', '')
        val = val.replace(',', '.')
        return (float(val) / fps) * 1000
    if RE_FLOAT_WCOMMA3.match(val):
        val = val.replace(',', '')
        return (float(val) / fps) * 1000
    return 0


def to_duration(val: typing.Any) -> str | None:
    """
    Convert milliseconds to duration string.
    Format:
        HH:MM:SS.MS
    If value is a str, its format will be tested.
    If it fits the schema, it will be returned,
    if not, None will be returned.
    If value is an int or float, it will be converted.
    All other values will return None.
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            str | None: Converted value
    """
    if isinstance(val, str):
        if RE_HHMMSS_STRICT.match(val):
            return val
        result = RE_HHMMSSMS_STRICT.match(val)
        if result:
            return f'{result.group(1)}:{result.group(2)}:{result.group(3)}.{result.group(4)[0:3]}'
        result = RE_HHMMSSMS_ALT.match(val)
        if result:
            return f'{result.group(1)}:{result.group(2)}:{result.group(3)}.{result.group(4)[0:3]}'
        return None

    if isinstance(val, (int, float)):
        secs = int(val / 1000)
        ms = int(val - (secs * 1000))
        mins = int(secs / 60)
        secs = secs % 60
        hours = int(mins / 60)
        mins = mins % 60
        return f"{str(hours).rjust(2, '0')}:{str(mins).rjust(2, '0')}:{str(secs).rjust(2, '0')}.{str(ms).rjust(3, '0')}"
    
    return None


def to_date(
    val: typing.Any) -> datetime.date | None:
    """
    Convert string to date object.
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            datetime.date | None: Converted value
    """
    if isinstance(val, datetime.datetime):
        return val.date()
    if isinstance(val, datetime.date):
        return val
    if isinstance(val, datetime.time):
        return None
    if not isinstance(val, str):
        return None

    result = RE_DATE_F1.match(val)
    if result:
        return datetime.date(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3))
        )

    result = RE_DATE_F2.match(val)
    if result:
        return datetime.date(
            int(result.group(3)),
            int(result.group(2)),
            int(result.group(1))
        )
    
    return None


def to_time(
    val: typing.Any) -> datetime.time | None:
    """
    Convert string to time object.
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            datetime.time | None: Converted value
    """
    if isinstance(val, datetime.datetime):
        return val.time()
    if isinstance(val, datetime.date):
        return None
    if isinstance(val, datetime.time):
        return val
    if not isinstance(val, str):
        return None

    result = RE_TIME_F1.match(val)
    if result:
        return datetime.time(
            int(result.group(1)),
            int(result.group(2))
        )

    result = RE_TIME_F2.match(val)
    if result:
        return datetime.time(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3))
        )

    result = RE_TIME_F3.match(val)
    if result:
        return datetime.time(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3)),
            int(result.group(4))
        )
    
    return None


def to_datetime(
    val: typing.Any) -> datetime.datetime | None:
    """
    Convert string to datetime object.
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            datetime.datetime | None: Converted value
    """
    if isinstance(val, datetime.datetime):
        return val.time()
    if isinstance(val, datetime.date):
        return datetime.combine(val, datetime.min.time())
    if isinstance(val, datetime.time):
        return None
    if not isinstance(val, str):
        return None
    
    if RE_DATETIME_F01.match(val):
        return datetime.datetime.strptime(
            val,
            '%Z %Y-%m-%d %H:%M:%S'
        )
    
    result = RE_DATETIME_F11.match(val)
    if result:
        return datetime.datetime(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3)),
            int(result.group(4)),
            int(result.group(5))
        )

    result = RE_DATETIME_F12.match(val)
    if result:
        return datetime.datetime(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3)),
            int(result.group(4)),
            int(result.group(5)),
            int(result.group(6))
        )

    result = RE_DATETIME_F13.match(val)
    if result:
        return datetime.datetime(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3)),
            int(result.group(4)),
            int(result.group(5)),
            int(result.group(6)),
            int(result.group(7))
        )
    
    result = RE_DATETIME_F21.match(val)
    if result:
        return datetime.datetime(
            int(result.group(3)),
            int(result.group(2)),
            int(result.group(1)),
            int(result.group(4)),
            int(result.group(5))
        )

    result = RE_DATETIME_F22.match(val)
    if result:
        return datetime.datetime(
            int(result.group(3)),
            int(result.group(2)),
            int(result.group(1)),
            int(result.group(4)),
            int(result.group(5)),
            int(result.group(6))
        )

    result = RE_DATETIME_F23.match(val)
    if result:
        return datetime.datetime(
            int(result.group(3)),
            int(result.group(2)),
            int(result.group(1)),
            int(result.group(4)),
            int(result.group(5)),
            int(result.group(6)),
            int(result.group(7))
        )

    result = RE_DATE_F1.match(val)
    if result:
        return datetime.datetime(
            int(result.group(1)),
            int(result.group(2)),
            int(result.group(3)),
            0, 0, 0
        )

    result = RE_DATE_F2.match(val)
    if result:
        return datetime.datetime(
            int(result.group(3)),
            int(result.group(2)),
            int(result.group(1)),
            0, 0, 0
        )
    
    return None


def date_to_str(
    val: typing.Any,
    format: str = '%Y-%m-%d') -> str | None:
    """
    Convert datetime or date object to string
    
        Parameters:
            val (typing.Any): Value to convert
            format (str): Target format
        
        Returns:
            str | None: Converted value
    """
    if not isinstance(val, (datetime.datetime, datetime.date)):
        return None
    if isinstance(val, datetime.datetime):
        val = val.date()
    return val.strftime(format)


def time_to_str(
    val: typing.Any,
    format: str = '%H:%M:%S') -> str | None:
    """
    Convert datetime or time object to string
    
        Parameters:
            val (typing.Any): Value to convert
            format (str): Target format
        
        Returns:
            str | None: Converted value
    """
    if not isinstance(val, (datetime.datetime, datetime.time)):
        return None
    if isinstance(val, datetime.datetime):
        val = val.time()
    return val.strftime(format)


def datetime_to_str(
    val: typing.Any,
    format: str = '%Y-%m-%d %H:%M:%S') -> str | None:
    """
    Convert datetime or date object to string
    
        Parameters:
            val (typing.Any): Value to convert
            format (str): Target format
        
        Returns:
            str | None: Converted value
    """
    if not isinstance(val, (datetime.datetime, datetime.date)):
        return None
    if not isinstance(val, datetime.date):
        val = datetime.combine(val, datetime.min.time())
    return val.strftime(format)


def ratio_to_number(
    val: typing.Any,
    default: int | float | None = None) -> int | float | None:
    """
    Convert ratio to number
    
        Parameters:
            val (typing.Any): Value to convert
            default (int | float | None): Default value if conversion fails
        
        Returns:
            int | float | None: Converted value
    """
    if val is None:
        return default
    if isinstance(val, (int, float)):
        return val
    if not isinstance(val, str):
        return default
    
    result = RE_RATIO.match(val)
    if not result:
        return default
    
    num1 = to_float(result.group(1))
    num2 = to_float(result.group(2))
    if num1 == 0:
        return 0
    if num2 == 0:
        return default
    return num1 / num2


def to_surround_type(
    val: typing.Any,
    num_channels: int | None) -> str | None:
    """
    Convert to surround type
    
        Parameters:
            val (typing.Any): Value to convert
        
        Returns:
            str | None: Converted value
    """
    if isinstance(num_channels, int):
        if num_channels in SURROUND_CHANNELS_MAP:
            num_channels = SURROUND_CHANNELS_MAP[num_channels]
        else:
            num_channels = f'{str(val)}.0'
    
    if val is None:
        return num_channels
    if isinstance(val, int):
        return f'{str(val)}.0'
    if isinstance(val, float):
        val1 = int(val)
        val2 = int((val - val1) * 10)
        return f'{str(val1)}.{str(val2)}'
    if not isinstance(val, str):
        return num_channels
    
    result = RE_SURROUND_TYPES.match(val)
    if not result:
        return num_channels

    return result.group(1).lower()





UTILS_FUNCTION_MAP = {
    'get_uuid': get_uuid,
    'to_bool': to_bool,
    'is_false': is_false,
    'is_true': is_true,
    'to_tristate': to_tristate,
    'to_number': to_number,
    'to_int': to_int,
    'to_int_or_bool': to_int_or_bool,
    'to_float': to_float,
    'to_float_or_bool': to_float_or_bool,
    'is_positive': is_positive,
    'is_negative': is_negative,
    'get_numerator': get_numerator,
    'get_denominator': get_denominator,
    'abs': to_abs,
    'to_str': to_str,
    'to_str_from_selection': to_str_from_selection,
    'is_empty': is_empty,
    'str_is_empty': str_is_empty,
    'str_is_not_empty': str_is_not_empty,
    'to_dict': to_dict,
    'to_dict_or_bool': to_dict_or_bool,
    'to_list': to_list,
    'to_list_from_selection': to_list_from_selection,
    'remove_trailling_comma': remove_trailling_comma,
    'to_msecs': to_msecs,
    'to_musecs': to_musecs,
    'frames_to_msecs': frames_to_msecs,
    'to_duration': to_duration,
    'to_date': to_date,
    'to_time': to_time,
    'to_datetime': to_datetime,
    'date_to_str': datetime_to_str,
    'time_to_str': datetime_to_str,
    'datetime_to_str': datetime_to_str,
    'ratio_to_number': ratio_to_number,
    'to_surround_type': to_surround_type
}







####################################################################################################
# Testing

if __name__ == '__main__':
    pass
    

